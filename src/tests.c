#include "tests.h"

int HEAP_SIZE = 64000;

static struct block_header *block_get_header(void *contents) {
    return (struct block_header *)(((uint8_t *)contents) -
                                   offsetof(struct block_header, contents));
}

static bool validHeap(void *heap){
    if (!heap) {
        fprintf(stderr, "%s", "The heap failed to initialize, probably not in the mood");
        return false;
    }
    return true;
}

static void test1(){

    fprintf(stdout, "%s", "--------- Test 1 (Usual allocate) ---------\n\n");
    void *heap = heap_init(HEAP_SIZE);

    if (validHeap(heap)) {
        return;
    }

    debug_heap(stdout, heap);
    void* regularBlock = _malloc(2103);
    debug_heap(stdout, heap);

    if (!regularBlock){
        fprintf(stderr, "%s", "Memory wasn't allocated.");
        return;
    }

    _free(regularBlock);
    debug_heap(stdout, heap);


    munmap(heap, size_from_capacity((block_capacity){.bytes = 64000}).bytes);
    fprintf(stdout, "%s", "Success!");
}

static void test2() {

    fprintf(stdout, "%s", "--------- Test 2 (Usual allocate) ---------\n\n");
    void *heap = heap_init(HEAP_SIZE);

    if (validHeap(heap)) {
        return;
    }

    debug_heap(stdout, heap);
    void* regularBlock1 = _malloc(2103);
    void* regularBlock2 = _malloc(2103);
    void* regularBlock3 = _malloc(2103);
    void* regularBlock4 = _malloc(2103);
    debug_heap(stdout, heap);

    if (!regularBlock1 || !regularBlock2 || !regularBlock3 || !regularBlock4){
        fprintf(stderr, "%s", "Memory wasn't allocated, it's tired..");
        return;
    }

    _free(regularBlock3);
    debug_heap(stdout, heap);

    if (!regularBlock1 || !regularBlock2 || !regularBlock4){
        fprintf(stderr, "%s", "We have a problem... Blocks that are not guilty have suffered");
        return;
    }

    _free(regularBlock1);
    _free(regularBlock2);
    _free(regularBlock4);
    debug_heap(stdout, heap);

    munmap(heap, size_from_capacity((block_capacity){.bytes = 64000}).bytes);
    fprintf(stdout, "%s", "Success!");
}


static void test3() {

    fprintf(stdout, "%s", "--------- Test 3 (Usual allocate) ---------\n\n");
    void *heap = heap_init(HEAP_SIZE);

    if (validHeap(heap)) {
        return;
    }

    debug_heap(stdout, heap);
    void* regularBlock1 = _malloc(2103);
    void* regularBlock2 = _malloc(2103);
    void* regularBlock3 = _malloc(2103);
    void* regularBlock4 = _malloc(2103);
    debug_heap(stdout, heap);

    if (!regularBlock1 || !regularBlock2 || !regularBlock3 || !regularBlock4){
        fprintf(stderr, "%s", "Memory wasn't allocated, it's tired..");
        return;
    }

    _free(regularBlock3);
    _free(regularBlock2);
    debug_heap(stdout, heap);

    if (!regularBlock1 || !regularBlock4){
        fprintf(stderr, "%s", "We have a problem... Blocks that are not guilty have suffered");
        return;
    }

    _free(regularBlock1);
    _free(regularBlock4);
    debug_heap(stdout, heap);

    munmap(heap, size_from_capacity((block_capacity){.bytes = 64000}).bytes);
    fprintf(stdout, "%s", "Success!");
}

static void test4() {

    fprintf(stdout, "%s", "--------- Test 4 (The memory is gone...) ---------\n\n");
    void *heap = heap_init(HEAP_SIZE);

    if (validHeap(heap)) {
        return;
    }

    debug_heap(stdout, heap);

    debug_heap(stdout, heap);
    void* regularBlock1 = _malloc(2103);
    void* regularBlock2 = _malloc(2103);
    void* regularBlock3 = _malloc(2103);
    _free(regularBlock2);
    debug_heap(stdout, heap);

    void* regularBlock4 = _malloc(59000);
    if (!regularBlock4) {
        fprintf(stderr, "%s", "Block does not fit");
        return;
    }
    debug_heap(stdout, heap);

    _free(regularBlock1);
    _free(regularBlock4);
    _free(regularBlock3);
    munmap(heap, size_from_capacity((block_capacity){.bytes = 64000}).bytes);
    fprintf(stdout, "%s", "Success!");
}

static void test5() {

    fprintf(stdout, "%s", "--------- Test 5 (The memory is gone...) ---------\n\n");
    void *heap = heap_init(HEAP_SIZE);

    if (validHeap(heap)) {
        return;
    }

    debug_heap(stdout, heap);
    void* regularBlock = _malloc(70000);
    if (!regularBlock) {
        fprintf(stderr, "%s", "Memory allocate failed");
        return;
    }

    debug_heap(stdout, heap);
    _free(regularBlock);
    debug_heap(stdout, heap);


    munmap(heap, size_from_capacity((block_capacity){.bytes = 70000}).bytes);
    fprintf(stdout, "%s", "Success!");
}

void startTest() {
    test1();
    test2();
    test3();
    test4();
    test5();
}
