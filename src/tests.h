
#ifndef ASSIGNMENT_4_MEMORY_ALLOCATOR_TESTS_H
#define ASSIGNMENT_4_MEMORY_ALLOCATOR_TESTS_H


#include "mem.h"
#include "mem_internals.h"
#include "util.h"

#include <stdio.h>

void startTest(void);

#endif //ASSIGNMENT_4_MEMORY_ALLOCATOR_TESTS_H
